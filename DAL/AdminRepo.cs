﻿using DAL.Repo;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
namespace DAL
{
    public class AdminMainProgress
    {
        private int _login;

        public List<PanelMenu> AllPanelMenu { get; set; }
        public List<PanelUsers> AllPanelUsers { get; set; }
        public List<SocialMedia> AllSocialMedia { get; set; }
        public List<MultiPreferences> AllMultiPreferences { get; set; }
        public List<Languages> AllLanguages { get; set; }
        public List<Slider> AllSlider { get; set; }
        public List<Pages> AllPages { get; set; }
        public List<Translates> AllTranslates { get; set; }
        public List<Campaigns> AllCampaigns { get; set; }
        public List<ContactForms> AllContactForms { get; set; }
        public List<Stores> AllStores { get; set; }
        public List<Catalogs> AllCatalogs { get; set; }
        public List<ProductGroups> AllProductGroups { get; set; }
        public List<CitiesAndCounties> AllCitiesAndCounties { get; set; }
        public List<StoreGalleries> AllStoreGalleries { get; set; }
        public MultiPreferences MultiPreferences { get; set; }
        public Pages Pages { get; set; }
        public Campaigns Campaigns { get; set; }
        public Stores Stores { get; set; }

        DatabaseConnection db = new DatabaseConnection();

        public AdminMainProgress(object login)
        {
            this._login = login.ObjectConvertToInt();
        }

        public static PanelUsers AdminLogin(string email, string password, DatabaseConnection db)
        {
            string hashedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "SHA1");
            var user = db.PanelUsers.Where(x => x.Email == email && x.Password == hashedPassword && x.Active == true && x.Deleted == false).FirstOrDefault();
            return user;
        }

        //PanelUser Start
        public List<PanelUsers> GetPanelUsers()
        {
            var list = db.PanelUsers.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void PanelUsersSave(string Email, string Password, string Name, string Surname, int Role, int Owner, string Active, object login, DatabaseConnection db)
        {
            var isHave = db.PanelUsers.Where(x => x.Email == Email && x.Deleted == false).FirstOrDefault();

            if (isHave == null)
            {
                string hashedPassword = FormsAuthentication.HashPasswordForStoringInConfigFile(Password, "SHA1");
                var DataActive = false;
                if (Active == "0")
                    DataActive = true;

                var paneluser = new PanelUsers();
                paneluser.Email = Email;
                paneluser.Password = hashedPassword;
                paneluser.Name = Name;
                paneluser.Surname = Surname;
                paneluser.Role = Role;
                paneluser.Owner = Owner;
                paneluser.CreatedDate = DateTime.Now;
                paneluser.Active = DataActive;
                paneluser.Deleted = false;
                db.PanelUsers.Add(paneluser);
                db.SaveChanges();
            }
        }

        public static void PanelUsersUpdate(string Id, string Email, string Name, string Surname, int Role, int Owner, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var isHave = db.PanelUsers.Where(x => x.Email == Email && x.Deleted == false && x.Id != ConvertId).FirstOrDefault();

            if (isHave == null)
            {
                var DataActive = false;
                if (Active == "0")
                    DataActive = true;

                var paneluser = db.PanelUsers.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
                if (paneluser != null)
                {
                    paneluser.Email = Email;
                    paneluser.Name = Name;
                    paneluser.Surname = Surname;
                    paneluser.Role = Role;
                    paneluser.Owner = Owner;
                    paneluser.UpdatedDate = DateTime.Now;
                    paneluser.Active = DataActive;
                    db.SaveChanges();
                }
            }
        }

        public static string PanelUsersDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.PanelUsers.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool PanelUsersIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var paneluserUpdate = db.PanelUsers.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            paneluserUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }
        //PanelUser End

        //PanelMenu Start
        public List<PanelMenu> GetPanelMenu()
        {
            var list = db.PanelMenu.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void PanelMenuSave(int ParentId, string Name, string Url, string Redirect, int Access, string Active, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var panelmenu = new PanelMenu();
            panelmenu.ParentId = ParentId;
            panelmenu.Name = Name;
            panelmenu.Url = Url;
            panelmenu.Redirect = Redirect;
            panelmenu.Icon = "dash";
            panelmenu.Access = Access;
            panelmenu.Order = db.PanelMenu.Count() + 1;
            panelmenu.Active = DataActive;
            panelmenu.Deleted = false;
            db.PanelMenu.Add(panelmenu);
            db.SaveChanges();
        }

        public static void PanelMenuUpdate(string Id, int ParentId, string Name, string Url, string Redirect, int Access, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var panelmenu = db.PanelMenu.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            if (panelmenu != null)
            {
                panelmenu.ParentId = ParentId;
                panelmenu.Name = Name;
                panelmenu.Url = Url;
                panelmenu.Redirect = Redirect;
                panelmenu.Icon = "dash";
                panelmenu.Access = Access;
                panelmenu.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string PanelMenuDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.PanelMenu.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool PanelMenuIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var panelmenuUpdate = db.PanelMenu.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            panelmenuUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool PanelMenuSort(List<int> ids, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.PanelMenu.Where(x => x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //PanelMenu End

        //SocialMedia Start
        public List<SocialMedia> GetSocialMedia()
        {
            var list = db.SocialMedia.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void SocialMediaSave(string Name, string Image, string Link, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var socialmedia = new SocialMedia();
            socialmedia.Name = Name;
            socialmedia.Image = Image;
            socialmedia.Link = Link;
            socialmedia.Order = db.SocialMedia.Count() + 1;
            socialmedia.CreatedDate = DateTime.Now;
            socialmedia.Owner = Owner;
            socialmedia.Active = DataActive;
            socialmedia.Deleted = false;
            db.SocialMedia.Add(socialmedia);
            db.SaveChanges();
        }

        public static void SocialMediaUpdate(string Id, string Name, string Image, string Link, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var socialmedia = db.SocialMedia.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            if (socialmedia != null)
            {
                socialmedia.Name = Name;
                socialmedia.Image = Image;
                socialmedia.Link = Link;
                socialmedia.UpdatedDate = DateTime.Now;
                socialmedia.Owner = Owner;
                socialmedia.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string SocialMediaDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.SocialMedia.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool SocialMediaIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var socialmediaUpdate = db.SocialMedia.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            socialmediaUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool SocialMediaSort(List<int> ids, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.SocialMedia.Where(x => x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //SocialMedia End

        //Languages Start
        public List<Languages> GetLanguages()
        {
            var list = db.Languages.Where(x => x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public static void LanguagesSave(string Name, string ShortName, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var languages = new Languages();
            languages.Name = Name;
            languages.ShortName = ShortName;
            languages.Order = db.Languages.Count() + 1;
            languages.CreatedDate = DateTime.Now;
            languages.Owner = Owner;
            languages.Active = DataActive;
            languages.Deleted = false;
            db.Languages.Add(languages);
            db.SaveChanges();
        }

        public static void LanguagesUpdate(string Id, string Name, string ShortName, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var languages = db.Languages.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            if (languages != null)
            {
                languages.Name = Name;
                languages.ShortName = ShortName;
                languages.UpdatedDate = DateTime.Now;
                languages.Owner = Owner;
                languages.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string LanguagesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Languages.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool LanguagesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var languagesUpdate = db.Languages.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            languagesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool LanguagesSort(List<int> ids, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.Languages.Where(x => x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //Languages End

        public object SelectLanguage(object LanguageId)
        {
            if (LanguageId == null)
            {
                LanguageId = 1;
            }

            return LanguageId;
        }

        //MultiPreferences Start
        public List<MultiPreferences> GetMultiPreferences()
        {
            var list = db.MultiPreferences.ToList();
            return list;
        }

        public static void MultiPreferencesUpdate(int LanguageId, string Title, string Description, string Keywords, string Phone, string Email, string AdvertisingBanner, string TopLink, int Owner, object login, DatabaseConnection db)
        {
            var multipreferences = db.MultiPreferences.Where(x => x.LanguageId == LanguageId).FirstOrDefault();
            if (multipreferences != null)
            {
                multipreferences.LanguageId = LanguageId;
                multipreferences.Title = Title;
                multipreferences.Description = Description;
                multipreferences.Keywords = Keywords;
                multipreferences.Phone = Phone;
                multipreferences.Email = Email;
                multipreferences.AdvertisingBanner = AdvertisingBanner;
                multipreferences.TopLink = TopLink;
                multipreferences.UpdatedDate = DateTime.Now;
                multipreferences.Owner = Owner;
                db.SaveChanges();
            }
            else
            {
                var multipreferences2 = new MultiPreferences();
                multipreferences2.LanguageId = LanguageId;
                multipreferences2.Title = Title;
                multipreferences2.Description = Description;
                multipreferences2.Keywords = Keywords;
                multipreferences2.Phone = Phone;
                multipreferences2.Email = Email;
                multipreferences2.AdvertisingBanner = AdvertisingBanner;
                multipreferences.TopLink = TopLink;
                multipreferences2.UpdatedDate = DateTime.Now;
                multipreferences2.Owner = Owner;
                db.MultiPreferences.Add(multipreferences2);
                db.SaveChanges();
            }
        }
        //MultiPreferences End

        //Pages Start
        public List<Pages> GetPages()
        {
            var list = db.Pages.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void PagesSave(int DataId, int LanguageId, string Title, string ShortContent, string Content, string Banner, string Thumbnail, string Image, string Positions, string View, string FriendlyUrl, string Keywords, string Description, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;

            if (Active == "0")
                DataActive = true;

            var pages = new Pages();
            if (DataId != 0) pages.DataId = DataId;
            pages.LanguageId = LanguageId;
            pages.Title = Title;
            pages.ShortContent = ShortContent;
            pages.Content = Content;
            pages.Banner = Banner;
            pages.Thumbnail = Thumbnail;
            pages.Image = Image;
            pages.Positions = Positions;
            pages.View = View;
            pages.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            pages.Keywords = Keywords;
            pages.Description = Description;
            pages.Order = db.Pages.Count() + 1;
            pages.CreatedDate = DateTime.Now;
            pages.Owner = Owner;
            pages.Active = DataActive;
            pages.Deleted = false;

            //Friendly Url
            if (FriendlyUrl == string.Empty)
            {
                FriendlyUrl = Title;
            }

            var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            int HaveUrl = db.Pages.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

            if (HaveUrl == 0)
            {
                pages.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            }
            else
            {
                for (int i = 0; i < 999; i++)
                {
                    HaveUrl = db.Pages.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                    if (HaveUrl == 0)
                    {
                        pages.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                        break;
                    }
                }
            }
            //Friendly Url

            db.Pages.Add(pages);
            db.SaveChanges();

            if (DataId == 0)
            {
                var pages2 = db.Pages.Where(x => x.Id == pages.Id).FirstOrDefault();
                pages2.DataId = pages.Id;
                db.SaveChanges();
            }
        }

        public static void PagesUpdate(string Id, int DataId, int LanguageId, string Title, string ShortContent, string Content, string Banner, string Thumbnail, string Image, string Positions, string View, string FriendlyUrl, string Keywords, string Description, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;

            if (Active == "0")
                DataActive = true;

            var pages = db.Pages.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (pages != null)
            {
                pages.DataId = DataId;
                pages.Title = Title;
                pages.ShortContent = ShortContent;
                pages.Content = Content;
                pages.Banner = Banner;
                pages.Thumbnail = Thumbnail;
                pages.Image = Image;
                pages.Positions = Positions;
                pages.View = View;
                pages.Keywords = Keywords;
                pages.Description = Description;
                pages.UpdatedDate = DateTime.Now;
                pages.Owner = Owner;
                pages.Active = DataActive;

                //Friendly Url
                if (FriendlyUrl == string.Empty)
                {
                    FriendlyUrl = Title;
                }

                var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                int HaveUrl = db.Pages.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

                if (HaveUrl == 0)
                {
                    pages.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                }
                else
                {
                    for (int i = 0; i < 999; i++)
                    {
                        HaveUrl = db.Pages.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                        if (HaveUrl == 0)
                        {
                            pages.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                            break;
                        }
                    }
                }
                //Friendly Url

                db.SaveChanges();
            }
        }

        public static string PagesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Pages.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool PagesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var pagesUpdate = db.Pages.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            pagesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool PagesSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.Pages.Where(x => x.LanguageId == LanguageId && x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //Pages End

        //Translates Start
        public List<Translates> GetTranslates()
        {
            var list = db.Translates.ToList();
            return list;
        }

        public static void TranslatesSave(int DataId, int LanguageId, string Description, string Text, int Owner, object login, DatabaseConnection db)
        {
            var translates = new Translates();
            if (DataId != 0) translates.DataId = DataId;
            translates.LanguageId = LanguageId;
            translates.Description = Description;
            translates.Text = Text;
            translates.CreatedDate = DateTime.Now;
            translates.Owner = Owner;
            db.Translates.Add(translates);
            db.SaveChanges();

            if (DataId == 0)
            {
                var translates2 = db.Translates.Where(x => x.Id == translates.Id).FirstOrDefault();
                translates2.DataId = translates.Id;
                db.SaveChanges();
            }
        }

        public static void TranslatesUpdate(string Id, int DataId, int LanguageId, string Description, string Text, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();

            var translates = db.Translates.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId).FirstOrDefault();
            if (translates != null)
            {
                translates.DataId = DataId;
                translates.Description = Description;
                translates.Text = Text;
                translates.UpdatedDate = DateTime.Now;
                translates.Owner = Owner;
                db.SaveChanges();
            }
        }
        //Translates End

        //Campaigns Start
        public List<Campaigns> GetCampaigns()
        {
            var list = db.Campaigns.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void CampaignsSave(int DataId, int LanguageId, string Title, string ShortContent, string Content, string Image, string Promotion, string PromotionImage, string Price, string Hashtags, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var DataPromotion = false;
            if (Promotion == "0")
                DataPromotion = true;

            var campaigns = new Campaigns();
            if (DataId != 0) campaigns.DataId = DataId;
            campaigns.LanguageId = LanguageId;
            campaigns.Title = Title;
            campaigns.ShortContent = ShortContent;
            campaigns.Content = Content;
            campaigns.Image = Image;
            campaigns.Promotion = DataPromotion;
            campaigns.PromotionImage = PromotionImage;
            campaigns.Price = Price;
            campaigns.Hashtags = Hashtags;
            campaigns.CreatedDate = DateTime.Now;
            campaigns.Owner = Owner;
            campaigns.Active = DataActive;
            campaigns.Deleted = false;
            db.Campaigns.Add(campaigns);
            db.SaveChanges();

            if (DataId == 0)
            {
                var campaigns2 = db.Campaigns.Where(x => x.Id == campaigns.Id).FirstOrDefault();
                campaigns2.DataId = campaigns.Id;
                db.SaveChanges();
            }
        }

        public static void CampaignsUpdate(string Id, int DataId, int LanguageId, string Title, string ShortContent, string Content, string Image, string Promotion, string PromotionImage, string Price, string Hashtags, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();

            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var DataPromotion = false;
            if (Promotion == "0")
                DataPromotion = true;

            var campaigns = db.Campaigns.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (campaigns != null)
            {
                campaigns.DataId = DataId;
                campaigns.Title = Title;
                campaigns.ShortContent = ShortContent;
                campaigns.Content = Content;
                campaigns.Image = Image;
                campaigns.Promotion = DataPromotion;
                campaigns.PromotionImage = PromotionImage;
                campaigns.Price = Price;
                campaigns.Hashtags = Hashtags;
                campaigns.UpdatedDate = DateTime.Now;
                campaigns.Owner = Owner;
                campaigns.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string CampaignsDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Campaigns.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool CampaignsIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var campaignsUpdate = db.Campaigns.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            campaignsUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }
        //Campaigns End

        //Slider Start
        public List<Slider> GetSlider()
        {
            var list = db.Slider.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void SliderSave(int LanguageId, string Title, string Text, string Image, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var slider = new Slider();
            slider.LanguageId = LanguageId;
            slider.Title = Title;
            slider.Text = Text;
            slider.Image = Image;
            slider.Order = db.Slider.Count() + 1;
            slider.CreatedDate = DateTime.Now;
            slider.Owner = Owner;
            slider.Active = DataActive;
            slider.Deleted = false;
            db.Slider.Add(slider);
            db.SaveChanges();
        }

        public static void SliderUpdate(string Id, int LanguageId, string Title, string Text, string Image, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var slider = db.Slider.Where(x => x.Id == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (slider != null)
            {
                slider.Title = Title;
                slider.Text = Text;
                slider.Image = Image;
                slider.UpdatedDate = DateTime.Now;
                slider.Owner = Owner;
                slider.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string SliderDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Slider.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool SliderIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var sliderUpdate = db.Slider.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            sliderUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool SliderSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.Slider.Where(x => x.LanguageId == LanguageId && x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //Slider End

        //Stores Start
        public List<Stores> GetStores()
        {
            var list = db.Stores.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void StoresSave(int DataId, int LanguageId, string Title, string Thumbnail, string Banner, string Email, string Phone, string Address, int County, string Location, string ProductGroups, string WorkingHours, string WorkingDays, string StoreManager, string StoreManagerImage, string FriendlyUrl, string Keywords, string Description, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var stores = new Stores();
            if (DataId != 0) stores.DataId = DataId;
            stores.LanguageId = LanguageId;
            stores.Title = Title;
            stores.Thumbnail = Thumbnail;
            stores.Banner = Banner;
            stores.Email = Email;
            stores.Phone = Phone;
            stores.Address = Address;
            stores.County = County;
            stores.Location = Location;
            stores.ProductGroups = ProductGroups;
            stores.WorkingHours = WorkingHours;
            stores.WorkingDays = WorkingDays;
            stores.StoreManager = StoreManager;
            stores.StoreManagerImage = StoreManagerImage;
            stores.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            stores.Keywords = Keywords;
            stores.Description = Description;
            stores.Order = db.Stores.Count() + 1;
            stores.CreatedDate = DateTime.Now;
            stores.Owner = Owner;
            stores.Active = DataActive;
            stores.Deleted = false;

            //Friendly Url
            if (FriendlyUrl == string.Empty)
            {
                FriendlyUrl = Title;
            }

            var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            int HaveUrl = db.Stores.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

            if (HaveUrl == 0)
            {
                stores.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
            }
            else
            {
                for (int i = 0; i < 999; i++)
                {
                    HaveUrl = db.Stores.Where(x => x.DataId != DataId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                    if (HaveUrl == 0)
                    {
                        stores.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                        break;
                    }
                }
            }
            //Friendly Url

            db.Stores.Add(stores);
            db.SaveChanges();

            if (DataId == 0)
            {
                var stores2 = db.Stores.Where(x => x.Id == stores.Id).FirstOrDefault();
                stores2.DataId = stores.Id;
                db.SaveChanges();
            }
        }

        public static void StoresUpdate(string Id, int DataId, int LanguageId, string Title, string Thumbnail, string Banner, string Email, string Phone, string Address, int County, string Location, string ProductGroups, string WorkingHours, string WorkingDays, string StoreManager, string StoreManagerImage, string FriendlyUrl, string Keywords, string Description, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var stores = db.Stores.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (stores != null)
            {
                stores.DataId = DataId;
                stores.Title = Title;
                stores.Thumbnail = Thumbnail;
                stores.Banner = Banner;
                stores.Email = Email;
                stores.Phone = Phone;
                stores.Address = Address;
                stores.County = County;
                stores.Location = Location;
                stores.ProductGroups = ProductGroups;
                stores.WorkingHours = WorkingHours;
                stores.WorkingDays = WorkingDays;
                stores.StoreManager = StoreManager;
                stores.StoreManagerImage = StoreManagerImage;
                stores.Keywords = Keywords;
                stores.Description = Description;
                stores.UpdatedDate = DateTime.Now;
                stores.Owner = Owner;
                stores.Active = DataActive;

                //Friendly Url
                if (FriendlyUrl == string.Empty)
                {
                    FriendlyUrl = Title;
                }

                var ConvertedUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                int HaveUrl = db.Stores.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == ConvertedUrl).Count();

                if (HaveUrl == 0)
                {
                    stores.FriendlyUrl = ExtensionMethod.ConvertToUrl(FriendlyUrl);
                }
                else
                {
                    for (int i = 0; i < 999; i++)
                    {
                        HaveUrl = db.Stores.Where(x => x.DataId != ConvertId && x.LanguageId == LanguageId && x.FriendlyUrl == (ConvertedUrl + "-")).Count();

                        if (HaveUrl == 0)
                        {
                            stores.FriendlyUrl = ExtensionMethod.ConvertToUrl(ConvertedUrl + "-");
                            break;
                        }
                    }
                }
                //Friendly Url

                db.SaveChanges();
            }
        }

        public static string StoresDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Stores.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool StoresIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var storesUpdate = db.Stores.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            storesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool StoresSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.Stores.Where(x => x.LanguageId == LanguageId && x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //Stores End

        //Catalogs Start
        public List<Catalogs> GetCatalogs()
        {
            var list = db.Catalogs.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void CatalogsSave(int DataId, int LanguageId, string Title, string Thumbnail, string Image, string Type, DateTime StartingDate, DateTime EndDate, string File, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var catalogs = new Catalogs();
            if (DataId != 0) catalogs.DataId = DataId;
            catalogs.LanguageId = LanguageId;
            catalogs.Thumbnail = Thumbnail;
            catalogs.Title = Title;
            catalogs.Image = Image;
            catalogs.Type = Type;
            catalogs.StartingDate = StartingDate;
            catalogs.EndDate = EndDate;
            catalogs.File = File;
            catalogs.CreatedDate = DateTime.Now;
            catalogs.Owner = Owner;
            catalogs.Active = DataActive;
            catalogs.Deleted = false;
            db.Catalogs.Add(catalogs);
            db.SaveChanges();

            if (DataId == 0)
            {
                var catalogs2 = db.Catalogs.Where(x => x.Id == catalogs.Id).FirstOrDefault();
                catalogs2.DataId = catalogs.Id;
                db.SaveChanges();
            }
        }

        public static void CatalogsUpdate(string Id, int DataId, int LanguageId, string Title, string Thumbnail, string Image, string Type, DateTime StartingDate, DateTime EndDate, string File, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var catalogs = db.Catalogs.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (catalogs != null)
            {
                catalogs.DataId = DataId;
                catalogs.Thumbnail = Thumbnail;
                catalogs.Title = Title;
                catalogs.Image = Image;
                catalogs.Type = Type;
                catalogs.StartingDate = StartingDate;
                catalogs.EndDate = EndDate;
                catalogs.File = File;
                catalogs.UpdatedDate = DateTime.Now;
                catalogs.Owner = Owner;
                catalogs.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string CatalogsDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.Catalogs.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool CatalogsIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var catalogsUpdate = db.Catalogs.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            catalogsUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }
        //Catalogs End

        //ProductGroups Start
        public List<ProductGroups> GetProductGroups()
        {
            var list = db.ProductGroups.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static void ProductGroupsSave(int DataId, int LanguageId, string Title, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var productgroups = new ProductGroups();
            if (DataId != 0) productgroups.DataId = DataId;
            productgroups.LanguageId = LanguageId;
            productgroups.Title = Title;
            productgroups.CreatedDate = DateTime.Now;
            productgroups.Owner = Owner;
            productgroups.Active = DataActive;
            productgroups.Deleted = false;
            db.ProductGroups.Add(productgroups);
            db.SaveChanges();

            if (DataId == 0)
            {
                var productgroups2 = db.ProductGroups.Where(x => x.Id == productgroups.Id).FirstOrDefault();
                productgroups2.DataId = productgroups.Id;
                db.SaveChanges();
            }
        }

        public static void ProductGroupsUpdate(string Id, int DataId, int LanguageId, string Title, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var productgroups = db.ProductGroups.Where(x => x.DataId == ConvertId && x.LanguageId == LanguageId && x.Deleted == false).FirstOrDefault();
            if (productgroups != null)
            {
                productgroups.DataId = DataId;
                productgroups.Title = Title;
                productgroups.UpdatedDate = DateTime.Now;
                productgroups.Owner = Owner;
                productgroups.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string ProductGroupsDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.ProductGroups.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool ProductGroupsIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var productgroupsUpdate = db.ProductGroups.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            productgroupsUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }
        //ProductGroups End

        //CitiesAndCounties Start
        public List<CitiesAndCounties> GetCitiesAndCounties()
        {
            var list = db.CitiesAndCounties.Where(x => x.Deleted == false).OrderBy(x => x.Title).ToList();
            return list;
        }

        public static void CitiesAndCountiesSave(int ParentId, string Title, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var citiesandcounties = new CitiesAndCounties();
            citiesandcounties.ParentId = ParentId;
            citiesandcounties.Title = Title;
            citiesandcounties.CreatedDate = DateTime.Now;
            citiesandcounties.Owner = Owner;
            citiesandcounties.Active = DataActive;
            citiesandcounties.Deleted = false;
            db.CitiesAndCounties.Add(citiesandcounties);
            db.SaveChanges();
        }

        public static void CitiesAndCountiesUpdate(string Id, int ParentId, string Title, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;
            if (Active == "0")
                DataActive = true;

            var citiesandcounties = db.CitiesAndCounties.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            if (citiesandcounties != null)
            {
                citiesandcounties.Id = ConvertId;
                citiesandcounties.ParentId = ParentId;
                citiesandcounties.Title = Title;
                citiesandcounties.UpdatedDate = DateTime.Now;
                citiesandcounties.Owner = Owner;
                citiesandcounties.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string CitiesAndCountiesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.CitiesAndCounties.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool CitiesAndCountiesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var citiesandcountiesUpdate = db.CitiesAndCounties.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            citiesandcountiesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }
        //CitiesAndCounties End

        //StoreGalleries Start
        public List<StoreGalleries> GetStoreGalleries()
        {
            var list = db.StoreGalleries.Where(x => x.Deleted == false).OrderBy(x => x.StoreId).ToList();
            return list;
        }

        public static void StoreGalleriesSave(int StoreId, string Image, string Active, int Owner, object login, DatabaseConnection db)
        {
            var DataActive = false;

            if (Active == "0")
                DataActive = true;

            var storegalleries = new StoreGalleries();
            storegalleries.StoreId = StoreId;
            storegalleries.Image = Image;
            storegalleries.Order = db.StoreGalleries.Count() + 1;
            storegalleries.CreatedDate = DateTime.Now;
            storegalleries.Owner = Owner;
            storegalleries.Active = DataActive;
            storegalleries.Deleted = false;

            db.StoreGalleries.Add(storegalleries);
            db.SaveChanges();
        }

        public static void StoreGalleriesUpdate(string Id, int StoreId, string Image, string Active, int Owner, object login, DatabaseConnection db)
        {
            var ConvertId = Id.ToInt32();
            var DataActive = false;

            if (Active == "0")
                DataActive = true;

            var storegalleries = db.StoreGalleries.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            if (storegalleries != null)
            {
                storegalleries.StoreId = StoreId;
                storegalleries.Image = Image;
                storegalleries.UpdatedDate = DateTime.Now;
                storegalleries.Owner = Owner;
                storegalleries.Active = DataActive;
                db.SaveChanges();
            }
        }

        public static string StoreGalleriesDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.StoreGalleries.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }

        public static bool StoreGalleriesIsActive(string Id, string Active, object login, DatabaseConnection db)
        {
            var ConvertId = Convert.ToInt32(Id);
            var storegalleriesUpdate = db.StoreGalleries.Where(x => x.Id == ConvertId && x.Deleted == false).FirstOrDefault();
            storegalleriesUpdate.Active = Convert.ToBoolean(Active);
            var result = db.SaveChanges();
            if (result == 1)
                return true;
            return false;
        }

        public static bool StoreGalleriesSort(List<int> ids, int LanguageId, object login, DatabaseConnection db)
        {
            int orderId = 1;
            var page = db.StoreGalleries.Where(x => x.Deleted == false).ToList();
            foreach (var id in ids)
            {
                var update = page.Where(x => x.Id == id).FirstOrDefault();
                update.Order = orderId;
                orderId++;
            }
            db.SaveChanges();
            return true;
        }
        //StoreGalleries End

        //ContactForms Start
        public List<ContactForms> GetContactForms()
        {
            var list = db.ContactForms.Where(x => x.Deleted == false).ToList();
            return list;
        }

        public static string ContactFormsDelete(object login, string Path, string QueryString, string id, DatabaseConnection db)
        {
            var url = String.Empty;
            url = Path;
            var action = QueryString;
            if (action == "delete")
            {
                var Id = id.IllegalCharRemove().ObjectConvertToInt();
                if (Id > 0)
                {
                    var which = login.ObjectConvertToInt();
                    var faq = db.ContactForms.Where(x => x.Id == Id && x.Deleted == false).FirstOrDefault();
                    if (faq != null)
                        faq.Deleted = true;
                    db.SaveChanges();
                    return url;
                }
            }
            return url = null;
        }
        //ContactForms End

        /////////////// <- Panel | Site -> ///////////////
        public List<Languages> SiteGetAllLanguages()
        {
            var list = db.Languages.Where(x => x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public Languages SiteGetLanguage(string ShortName)
        {
            var single = db.Languages.Where(x => x.ShortName == ShortName).FirstOrDefault();
            return single;
        }

        public List<Translates> SiteGetAllTranslates(int SiteLanguage)
        {
            var list = db.Translates.Where(x => x.LanguageId == SiteLanguage).ToList();
            return list;
        }

        public List<Slider> SiteGetAllSlider(int SiteLanguage)
        {
            var list = db.Slider.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<Pages> SiteGetAllPages(int SiteLanguage)
        {
            var list = db.Pages.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public Pages SiteGetPage(string friendlyUrl)
        {
            var single = db.Pages.Where(x => x.FriendlyUrl == friendlyUrl && x.Active == true && x.Deleted == false).FirstOrDefault();
            return single;
        }

        public List<SocialMedia> SiteGetAllSocialMedia()
        {
            var list = db.SocialMedia.Where(x => x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<Catalogs> SiteGetAllCatalogs(int SiteLanguage)
        {
            var list = db.Catalogs.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.StartingDate).ToList();
            return list;
        }

        public List<Campaigns> SiteGetAllCampaigns(int SiteLanguage)
        {
            var list = db.Campaigns.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<Stores> SiteGetAllStores(int SiteLanguage)
        {
            var list = db.Stores.Where(x => x.LanguageId == SiteLanguage && x.Active == true && x.Deleted == false).OrderBy(x => x.Order).ToList();
            return list;
        }

        public List<CitiesAndCounties> SiteGetAllCitiesAndCounties()
        {
            var list = db.CitiesAndCounties.Where(x => x.Active == true && x.Deleted == false).OrderBy(x => x.Title).ToList();
            return list;
        }

        public List<StoreGalleries> SiteGetAllStoreGalleries()
        {
            var list = db.StoreGalleries.Where(x => x.Active == true && x.Deleted == false).OrderBy(x => x.StoreId).ToList();
            return list;
        }

        public MultiPreferences SiteGetMultiPreferences(int SiteLanguage)
        {
            var single = db.MultiPreferences.Where(x => x.LanguageId == SiteLanguage).FirstOrDefault();
            return single;
        }

        //public static void ContactFormSave(string NameSurname, string Section, string Email, string Message, DatabaseConnection db)
        //{
        //    var contactform = new ContactForms();
        //    contactform.NameSurname = NameSurname;
        //    contactform.Section = Section;
        //    contactform.Email = Email;
        //    contactform.Message = Message;
        //    contactform.CreatedDate = DateTime.Now;
        //    contactform.Deleted = false;
        //    db.ContactForms.Add(contactform);
        //    db.SaveChanges();
        //}
    }
}