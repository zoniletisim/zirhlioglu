﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.Script.Serialization;
using System.Data;
using DAL;
using DAL.Repo;
using System.Data.SqlClient;
using System.Web.Caching;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;
using System.Collections;

namespace ZirhliogluCMS.Controllers
{
    public class AdminController : Controller
    {
        private DatabaseConnection db = new DatabaseConnection();

        //
        // GET: /Admin/

        public void CreateSession()
        {
            if (Session["AdminLanguage"] == null)
            {
                Session["AdminLanguage"] = 1;
            }
        }

        [ChildActionOnly]
        public ActionResult Header()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            return PartialView();
        }

        [ChildActionOnly]
        public ActionResult LeftMenu()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllPanelMenu = main.GetPanelMenu();
            return PartialView(main);
        }

        public ActionResult Index()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllPages = main.GetPages();
            return View(main);
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string email, string password)
        {
            var result = AdminMainProgress.AdminLogin(email, password, db);

            if (result != null)
            {
                Session.Add("LoginId", result.Id);
                Session.Add("Name", result.Name);
                Session.Add("Surname", result.Surname);
                Session.Add("Login", result.Role);
                Session.Add("AdminLanguage", 1);
                return Json(true);
            }

            return Json(false);
        }

        [HttpPost]
        public ActionResult LogOut()
        {
            Session["LoginId"] = null;
            Session["Name"] = null;
            Session["Surname"] = null;
            Session["Login"] = null;
            return Json(true);
        }

        [ChildActionOnly]
        public ActionResult ChangeLanguage()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllLanguages = main.GetLanguages();
            return PartialView(main);
        }

        [HttpPost]
        public ActionResult AdminLanguage(string language)
        {
            Session["AdminLanguage"] = language;
            return Json(language);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //PanelUsers Start
        public ActionResult PanelUsers()
        {
            CreateSession();
            var result = AdminMainProgress.PanelUsersDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("PanelUsers");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllPanelUsers = main.GetPanelUsers();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PanelUsersSave(FormCollection fc)
        {
            AdminMainProgress.PanelUsersSave(fc["Email"], fc["Password"], fc["Name"], fc["Surname"], int.Parse(fc["Role"]), int.Parse(Session["LoginId"].ToString()), fc["Active"], Session["Login"], db);
            return RedirectToAction("PanelUsers");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PanelUsersUpdate(FormCollection fc)
        {
            AdminMainProgress.PanelUsersUpdate(fc["Id"], fc["Email"], fc["Name"], fc["Surname"], int.Parse(fc["Role"]), int.Parse(Session["LoginId"].ToString()), fc["Active"], Session["Login"], db);
            return RedirectToAction("PanelUsers");
        }

        [HttpPost]
        public ActionResult PanelUsersIsActive(string Id, string Active)
        {
            var result = AdminMainProgress.PanelUsersIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }
        //PanelUsers End

        //PanelMenu Start
        public ActionResult PanelMenu()
        {
            CreateSession();
            var result = AdminMainProgress.PanelMenuDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("PanelMenu");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllPanelMenu = main.GetPanelMenu();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PanelMenuSave(FormCollection fc)
        {
            AdminMainProgress.PanelMenuSave(int.Parse(fc["ParentId"]), fc["Name"], fc["Url"], fc["Redirect"], int.Parse(fc["Access"]), fc["Active"], Session["Login"], db);
            return RedirectToAction("PanelMenu");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PanelMenuUpdate(FormCollection fc)
        {
            AdminMainProgress.PanelMenuUpdate(fc["Id"], int.Parse(fc["ParentId"]), fc["Name"], fc["Url"], fc["Redirect"], int.Parse(fc["Access"]), fc["Active"], Session["Login"], db);
            return RedirectToAction("PanelMenu");
        }

        [HttpPost]
        public ActionResult PanelMenuIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.PanelMenuIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult PanelMenuSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.PanelMenuSort(ids, Session["Login"], db);
            return Json("success");
        }
        //PanelMenu End

        //Languages Start
        public ActionResult Languages()
        {
            CreateSession();
            var result = AdminMainProgress.LanguagesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Languages");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllLanguages = main.GetLanguages();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult LanguagesSave(FormCollection fc)
        {
            AdminMainProgress.LanguagesSave(fc["Name"], fc["ShortName"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Languages");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult LanguagesUpdate(FormCollection fc)
        {
            AdminMainProgress.LanguagesUpdate(fc["Id"], fc["Name"], fc["ShortName"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Languages");
        }

        [HttpPost]
        public ActionResult LanguagesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.LanguagesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult LanguagesSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.LanguagesSort(ids, Session["Login"], db);
            return Json("success");
        }
        //Languages End

        //SocialMedia Start
        public ActionResult SocialMedia()
        {
            CreateSession();
            var result = AdminMainProgress.SocialMediaDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("SocialMedia");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllSocialMedia = main.GetSocialMedia();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SocialMediaSave(FormCollection fc)
        {
            AdminMainProgress.SocialMediaSave(fc["Name"], fc["Image"], fc["Link"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("SocialMedia");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SocialMediaUpdate(FormCollection fc)
        {
            AdminMainProgress.SocialMediaUpdate(fc["Id"], fc["Name"], fc["Image"], fc["Link"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("SocialMedia");
        }

        [HttpPost]
        public ActionResult SocialMediaIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.SocialMediaIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult SocialMediaSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.SocialMediaSort(ids, Session["Login"], db);
            return Json("success");
        }
        //SocialMedia End

        //MultiPreferences Start
        public ActionResult MultiPreferences()
        {
            CreateSession();
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllMultiPreferences = main.GetMultiPreferences();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult MultiPreferencesUpdate(FormCollection fc)
        {
            AdminMainProgress.MultiPreferencesUpdate(int.Parse(Session["AdminLanguage"].ToString()), fc["Title"], fc["Description"], fc["Keywords"], fc["Phone"], fc["Email"], fc["AdvertisingBanner"], fc["TopLink"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("MultiPreferences");
        }
        //MultiPreferences End

        //Pages Start
        public ActionResult Pages()
        {
            CreateSession();
            var result = AdminMainProgress.PagesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Pages");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllPages = main.GetPages();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PagesSave(FormCollection fc)
        {
            AdminMainProgress.PagesSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), fc["Title"], fc["ShortContent"], fc["Content"], fc["Banner"], fc["Thumbnail"], fc["Image"], fc["Positions"], fc["View"], fc["FriendlyUrl"], fc["Keywords"], fc["Description"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Pages");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PagesUpdate(FormCollection fc)
        {
            AdminMainProgress.PagesUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), fc["Title"], fc["ShortContent"], fc["Content"], fc["Banner"], fc["Thumbnail"], fc["Image"], fc["Positions"], fc["View"], fc["FriendlyUrl"], fc["Keywords"], fc["Description"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Pages");
        }

        [HttpPost]
        public ActionResult PagesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.PagesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult PagesSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.PagesSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //Pages End

        //Translates Start
        public ActionResult Translates()
        {
            CreateSession();
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllTranslates = main.GetTranslates();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult TranslatesSave(FormCollection fc)
        {
            AdminMainProgress.TranslatesSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), fc["Description"], fc["Text"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Translates");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult TranslatesUpdate(FormCollection fc)
        {
            AdminMainProgress.TranslatesUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), fc["Description"], fc["Text"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Translates");
        }
        //Translates End

        //Campaigns Start
        public ActionResult Campaigns()
        {
            CreateSession();
            var result = AdminMainProgress.CampaignsDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Campaigns");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllCampaigns = main.GetCampaigns();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CampaignsSave(FormCollection fc)
        {
            AdminMainProgress.CampaignsSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), fc["Title"], fc["ShortContent"], fc["Content"], fc["Image"], fc["Promotion"], fc["PromotionImage"], fc["Price"], fc["Hashtags"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Campaigns");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CampaignsUpdate(FormCollection fc)
        {
            AdminMainProgress.CampaignsUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), fc["Title"], fc["ShortContent"], fc["Content"], fc["Image"], fc["Promotion"], fc["PromotionImage"], fc["Price"], fc["Hashtags"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Campaigns");
        }

        [HttpPost]
        public ActionResult CampaignsIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.CampaignsIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }
        //Campaigns End

        //Slider Start
        public ActionResult Slider()
        {
            CreateSession();
            var result = AdminMainProgress.SliderDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Slider");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllSlider = main.GetSlider();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SliderSave(FormCollection fc)
        {
            AdminMainProgress.SliderSave(int.Parse(Session["AdminLanguage"].ToString()), fc["Title"], fc["Text"], fc["Image"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Slider");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SliderUpdate(FormCollection fc)
        {
            AdminMainProgress.SliderUpdate(fc["Id"], int.Parse(fc["LanguageId"].ToString()), fc["Title"], fc["Text"], fc["Image"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Slider");
        }

        [HttpPost]
        public ActionResult SliderIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.SliderIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult SliderSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.SliderSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //Slider End

        //Stores Start
        public ActionResult Stores()
        {
            CreateSession();
            var result = AdminMainProgress.StoresDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Stores");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllStores = main.GetStores();
            main.AllProductGroups = main.GetProductGroups();
            main.AllCitiesAndCounties = main.GetCitiesAndCounties();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult StoresSave(FormCollection fc)
        {
            AdminMainProgress.StoresSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), fc["Title"], fc["Thumbnail"], fc["Banner"], fc["Email"], fc["Phone"], fc["Address"], int.Parse(fc["County"].ToString()), fc["Location"], fc["ProductGroups"], fc["WorkingHours"], fc["WorkingDays"], fc["StoreManager"], fc["StoreManagerImage"], fc["FriendlyUrl"], fc["Keywords"], fc["Description"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Stores");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult StoresUpdate(FormCollection fc)
        {
            AdminMainProgress.StoresUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), fc["Title"], fc["Thumbnail"], fc["Banner"], fc["Email"], fc["Phone"], fc["Address"], int.Parse(fc["County"].ToString()), fc["Location"], fc["ProductGroups"], fc["WorkingHours"], fc["WorkingDays"], fc["StoreManager"], fc["StoreManagerImage"], fc["FriendlyUrl"], fc["Keywords"], fc["Description"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Stores");
        }

        [HttpPost]
        public ActionResult StoresIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.StoresIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult StoresSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.StoresSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //Stores End

        //Catalogs Start
        public ActionResult Catalogs()
        {
            CreateSession();
            var result = AdminMainProgress.CatalogsDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("Catalogs");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllCatalogs = main.GetCatalogs();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CatalogsSave(FormCollection fc)
        {
            AdminMainProgress.CatalogsSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), fc["Title"], fc["Thumbnail"], fc["Image"], fc["Type"], Convert.ToDateTime(fc["StartingDate"]), Convert.ToDateTime(fc["EndDate"]), fc["File"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Catalogs");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CatalogsUpdate(FormCollection fc)
        {
            AdminMainProgress.CatalogsUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), fc["Title"], fc["Thumbnail"], fc["Image"], fc["Type"], Convert.ToDateTime(fc["StartingDate"]), Convert.ToDateTime(fc["EndDate"]), fc["File"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("Catalogs");
        }

        [HttpPost]
        public ActionResult CatalogsIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.CatalogsIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }
        //Catalogs End

        //ProductGroups Start
        public ActionResult ProductGroups()
        {
            CreateSession();
            var result = AdminMainProgress.ProductGroupsDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("ProductGroups");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllProductGroups = main.GetProductGroups();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ProductGroupsSave(FormCollection fc)
        {
            AdminMainProgress.ProductGroupsSave(int.Parse(fc["DataId"].ToString()), int.Parse(Session["AdminLanguage"].ToString()), fc["Title"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("ProductGroups");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ProductGroupsUpdate(FormCollection fc)
        {
            AdminMainProgress.ProductGroupsUpdate(fc["Id"], int.Parse(fc["DataId"].ToString()), int.Parse(fc["LanguageId"].ToString()), fc["Title"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("ProductGroups");
        }

        [HttpPost]
        public ActionResult ProductGroupsIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.ProductGroupsIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }
        //ProductGroups End

        //CitiesAndCounties Start
        public ActionResult CitiesAndCounties()
        {
            CreateSession();
            var result = AdminMainProgress.CitiesAndCountiesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("CitiesAndCounties");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllCitiesAndCounties = main.GetCitiesAndCounties();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CitiesAndCountiesSave(FormCollection fc)
        {
            AdminMainProgress.CitiesAndCountiesSave(int.Parse(fc["ParentId"].ToString()), fc["Title"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("CitiesAndCounties");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CitiesAndCountiesUpdate(FormCollection fc)
        {
            AdminMainProgress.CitiesAndCountiesUpdate(fc["Id"], int.Parse(fc["ParentId"].ToString()), fc["Title"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("CitiesAndCounties");
        }

        [HttpPost]
        public ActionResult CitiesAndCountiesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.CitiesAndCountiesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }
        //CitiesAndCounties End

        //StoreGalleries Start
        public ActionResult StoreGalleries()
        {
            CreateSession();
            var result = AdminMainProgress.StoreGalleriesDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("StoreGalleries");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllStoreGalleries = main.GetStoreGalleries();
            main.AllStores = main.GetStores();
            return View(main);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult StoreGalleriesSave(FormCollection fc)
        {
            AdminMainProgress.StoreGalleriesSave(int.Parse(fc["StoreId"].ToString()), fc["Image"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("StoreGalleries");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult StoreGalleriesUpdate(FormCollection fc)
        {
            AdminMainProgress.StoreGalleriesUpdate(fc["Id"], int.Parse(fc["StoreId"].ToString()), fc["Image"], fc["Active"], int.Parse(Session["LoginId"].ToString()), Session["Login"], db);
            return RedirectToAction("StoreGalleries");
        }

        [HttpPost]
        public ActionResult StoreGalleriesIsActive(string Id, string Active)
        {
            CreateSession();
            var result = AdminMainProgress.StoreGalleriesIsActive(Id, Active, Session["Login"], db);
            if (result)
                return Json("success");
            return Json("unsuccess");
        }

        [HttpPost]
        public ActionResult StoreGalleriesSort(string Id)
        {
            JavaScriptSerializer sr = new JavaScriptSerializer();
            var ids = sr.Deserialize<List<int>>(Id);
            AdminMainProgress.StoreGalleriesSort(ids, int.Parse(Session["AdminLanguage"].ToString()), Session["Login"], db);
            return Json("success");
        }
        //StoreGalleries End

        //ContactForms Start
        public ActionResult ContactForms()
        {
            CreateSession();
            var result = AdminMainProgress.ContactFormsDelete(Session["Login"], Request.Url.AbsolutePath, Request.QueryString["action"], Request.QueryString["Id"], db);
            if (result != null)
                return RedirectToAction("ContactForms");

            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            main.AllContactForms = main.GetContactForms();
            return View(main);
        }
        //ContactForms End
    }
}