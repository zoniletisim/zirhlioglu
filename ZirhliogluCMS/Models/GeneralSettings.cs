﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace consulta.Models
{
   public enum WhichSite 
    {
        None=0,
        Consulta=1,
        ConsultaVergi=2,
        KinaliAda=3,
    }

   public enum Inbox
    {
        None=0,
        Email=1,
        WantOffer=2
    }

   public enum Content 
   {
        Page=0,
        News=1,
        BroadcastEducationalSeminars=2,
        JudicialDecisions=3
   }

   public enum UserRole 
   {
        None=0,
        Admin=1,
        User=2
   }

   public enum MailOrOffer 
   {
        None=0,
        Mail=1,
        Offer=2
   }
}