﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ZirhliogluCMS.Startup))]
namespace ZirhliogluCMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
