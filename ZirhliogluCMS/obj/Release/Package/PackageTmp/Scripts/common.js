﻿$(document).ready(function () {
    $('#ChangeLanguage').change(function () {

        $.ajax({
            url: '/Admin/AdminLanguage',
            type: 'POST',
            dataType: 'Json',
            data: { language: $(this).val() },
            success: function (data) {
            },
            error: function (err) {
            }
        });
        location.reload();
    });

    $('#login').click(function () {
        var email = $('#req1').val();
        var password = $('#req2').val();
        if (email != "" && password != "") {

            $.ajax({
                type: 'POST',
                url: '/Admin/Login',
                dataType: 'Json',
                data: { email: email, password: password },
                success: function (data) {
                    if (data == true)
                        window.location.href = "/Admin/Index";
                    else
                        alert("Kullanıcı adı veya şifre yanlış!");
                },
                error: function (err) {
                    alert(err);
                }
            });

        }
    });

    $('#logout').click(function () {
        $.ajax({
            type: 'POST',
            url: '/Admin/LogOut',
            dataType: 'Json',
            success: function (data) {
                if (data == true)
                    window.location = "/Admin/Login";
            },
            error: function (err) {
            }
        });
    });

    //PanelUsers End
    $('.panelusersIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/PanelUsersIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });
    //PanelUser End

    //PanelMenu End
    $('.panelmenuIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/PanelMenuIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#PanelMenuSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/PanelMenuSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //PanelMenu End

    //SocialMedia End
    $('.socialmediaIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/SocialMediaIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#SocialMediaSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/SocialMediaSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //SocialMedia End

    //Languages End
    $('.languagesIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/LanguagesIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#LanguagesSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/LanguagesSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //Languages End

    //Pages End
    $('.pagesIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/PagesIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#PagesSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/PagesSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //Pages End

    //Stores End
    $('.storesIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/StoresIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#StoresSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/StoresSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //Stores End

    //Campaigns End
    $('.campaignsIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/CampaignsIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#CampaignsSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/CampaignsSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //Campaigns End

    //Slider End
    $('.sliderIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/SliderIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#SliderSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/SliderSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //Slider End

    //Catalogs End
    $('.catalogsIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/CatalogsIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });
    //Catalogs End

    //ProductGroups End
    $('.productgroupsIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/ProductGroupsIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });
    //ProductGroups End

    //CitiesAndCounties End
    $('.citiesandcountiesIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/CitiesAndCountiesIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });
    //CitiesAndCounties End

    //StoreGalleries End
    $('.storegalleriesIsActive').live("change", function () {
        var id = $(this).prop('id');
        var ischeked = $(this).is(':checked');
        $.ajax({
            url: '/Admin/StoreGalleriesIsActive',
            type: 'POST',
            dataType: 'Json',
            data: { id: id, Active: ischeked },
            success: function (data) {
                console.log(data);
            }
        });
    });

    $('#StoreGalleriesSaveSort').click(function () {
        var Id = new Array();
        $('#sortable li').each(function (index, val) {
            if ($(this).attr('id') != undefined) {
                Id[index] = $(this).attr('id');
            }
        });
        $.ajax({
            url: '/Admin/StoreGalleriesSort',
            type: 'POST',
            dataType: 'json',
            data: { Id: JSON.stringify(Id) },
            success: function (data) {
                if (data == "success") {
                    alert("Başarılı");
                }
                else {
                    alert("Başarısız");
                }
            },
        });
    });
    //StoreGalleries End
});