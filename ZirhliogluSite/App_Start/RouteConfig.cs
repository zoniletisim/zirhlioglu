﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ZirhliogluSite
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            foreach (var item in DAL.Enums.PageTypes)
            {
                routes.MapRoute(
                name: item.Key,
                url: item.Value + "{subFriendlyUrl}/",
                defaults: new { controller = "Home", action = item.Key, subFriendlyUrl = UrlParameter.Optional }
                );
            }

            routes.MapRoute(
            name: "ChangeLanguage",
            url: "ChangeLanguage/{shortName}",
            defaults: new { controller = "Home", action = "ChangeLanguage", shortName = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}