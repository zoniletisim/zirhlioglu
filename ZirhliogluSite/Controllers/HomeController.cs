﻿using DAL;
using DAL.Repo;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace ZirhliogluSite.Controllers
{
    public class LanguageActionFilter : ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContextBase context = filterContext.RequestContext.HttpContext;
            string cookie = context.Request.Cookies["culture"] != null && context.Request.Cookies["culture"].Value != "" ? context.Request.Cookies["culture"].Value : ConfigurationManager.AppSettings["DefaultLanguage"];
            if (cookie != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(cookie);
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(cookie);
            }
        }
    }

    public class HomeController : Controller
    {
        private DatabaseConnection db = new DatabaseConnection();

        public void ChangeLanguage(string ShortName)
        {
            Response.Cookies.Remove("culture");
            Response.Cookies.Add(new HttpCookie("culture")
            {
                Name = "culture",
                Expires = DateTime.Now.AddDays(3),
                Path = "/",
                Value = ShortName
            });
            Response.Redirect("/");

            //AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            //int oldLang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            //int newLang = main.SiteGetLanguage(ShortName).Id;
            //Uri refUrl = Request.UrlReferrer;
            //string refUrl2 = refUrl != null ? refUrl.Host : "";
            //string curUrl = Request.Url.Host;
            //string friendlyUrl;
            //string subFriendlyUrl;

            //if (refUrl2 != curUrl)
            //{
            //    Response.Redirect("/");
            //}
            //else if (refUrl.Segments.Count() <= 1)
            //{
            //    Response.Redirect("/");
            //}
            //else
            //{
            //    if (refUrl.Segments[1].ToLower() == "sayfa/")
            //    {
            //        friendlyUrl = refUrl.Segments[2];
            //        subFriendlyUrl = refUrl.Segments.Count() > 3 ? "/" + refUrl.Segments[3] : string.Empty;
            //        main.AllPages = main.SiteGetAllPages(oldLang);
            //        var oldData = main.AllPages.Where(x => x.FriendlyUrl == friendlyUrl).FirstOrDefault();
            //        main.AllPages = main.SiteGetAllPages(newLang);
            //        var newData = main.AllPages.Where(x => x.DataId == oldData.DataId).FirstOrDefault();
            //        Response.Redirect(newData == null ? "/" : "/" + refUrl.Segments[1] + newData.FriendlyUrl + subFriendlyUrl);
            //    }
            //    else
            //    {
            //        Response.Redirect("/");
            //    }
            //}
        }

        public ActionResult _Header()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.MultiPreferences = main.SiteGetMultiPreferences(lang);
            main.AllPages = main.SiteGetAllPages(lang);
            return View(main);
        }

        public ActionResult _Footer()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.MultiPreferences = main.SiteGetMultiPreferences(lang);
            main.AllSocialMedia = main.SiteGetAllSocialMedia();
            main.AllPages = main.SiteGetAllPages(lang);
            return View(main);
        }

        public ActionResult Index()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.MultiPreferences = main.SiteGetMultiPreferences(lang);
            main.AllSlider = main.SiteGetAllSlider(lang);
            main.AllCatalogs = main.SiteGetAllCatalogs(lang);
            main.AllCampaigns = main.SiteGetAllCampaigns(lang);
            return View(main);
        }

        public ActionResult Catalogs()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.AllCatalogs = main.SiteGetAllCatalogs(lang);
            return View(main);
        }

        public ActionResult Campaigns()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.AllCampaigns = main.SiteGetAllCampaigns(lang);
            return View(main);
        }

        public ActionResult Stores()
        {
            AdminMainProgress main = new AdminMainProgress(Session["Login"]);
            var lang = main.SiteGetLanguage(Thread.CurrentThread.CurrentCulture.Name).Id;
            main.AllStores = main.SiteGetAllStores(lang);
            main.AllCitiesAndCounties = main.SiteGetAllCitiesAndCounties();
            main.AllStoreGalleries = main.SiteGetAllStoreGalleries();
            return View(main);
        }
    }
}